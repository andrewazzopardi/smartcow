type Video = {
  id: number;
  userId: number;
  name: string;
  description: string;
  actorId: number;
  script: string;
  alignment: string;
  background: string;
  voiceId: number;
  tagList: string[];
}
