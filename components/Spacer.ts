import styled from 'styled-components';

type Props = {
  height?: number;
  width?: number;
}

const Spacer = styled.div<Props>`
  height: ${(props) => props.height || 1}px;
  width: ${(props) => props.width || 1}px;
`;

export default Spacer;
