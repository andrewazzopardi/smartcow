import React from 'react';
import styled from 'styled-components';
import Button from '../../components/Form/Button';
import Link from '../../components/Link';
import Page, { Content, Header, HeaderActions } from '../../components/Page/Page';
import SmallPreview from '../../components/Video/SmallPreview';
import privatePage from '../../hoc/privatePage';

const StyledContent = styled(Content)`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(280px, 1fr));
  grid-gap: 20px;
`;

function BrowsePage() {
  const videos = localStorage.getItem('videos') || '[]';
  const parsedVideos = JSON.parse(videos);

  return (
    <Page>
      <Header>
        Saved Videos
        <HeaderActions>
          <Link href="/video">
            <Button>
              Create New
            </Button>
          </Link>
        </HeaderActions>
      </Header>
      <StyledContent>
        {parsedVideos.map((video) => (
          <div key={video.id}>
            <Link href={`/video/${video.id}`}>
              <SmallPreview video={video} />
            </Link>
          </div>
        ))}
      </StyledContent>
    </Page>
  );
}

export default privatePage(BrowsePage);
