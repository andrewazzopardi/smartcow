/* eslint-disable import/prefer-default-export */
const acceptTemplate = (string: string[], ...args: any[]) => `${string[0]}${args.map((arg, index) => `${arg}${string[index + 1]}`)}`;
export { acceptTemplate };
