import React from 'react';
import MyBilling from '../components/Account/MyBilling';
import MyPlan from '../components/Account/MyPlan';
import ProfileSettings from '../components/Account/ProfileSettings';
import Button from '../components/Form/Button';
import Page, { Content, Header, HeaderActions } from '../components/Page/Page';
import Tab from '../components/Tabs/Tab';
import Tabs from '../components/Tabs/Tabs';
import privatePage from '../hoc/privatePage';
import { useAuthContext } from '../hooks/contexts';

function AccountPage() {
  const { logout } = useAuthContext();

  return (
    <Page>
      <Header>
        My Account
        <HeaderActions>
          <Button onClick={logout} noBackground color="red">
            Logout
          </Button>
        </HeaderActions>
      </Header>
      <Content>
        <Tabs>
          <Tab name="Profile">
            <ProfileSettings />
          </Tab>
          <Tab name="My Plan">
            <MyPlan />
          </Tab>
          <Tab name="Billing">
            <MyBilling />
          </Tab>
        </Tabs>
      </Content>
    </Page>
  );
}

export default privatePage(AccountPage);
