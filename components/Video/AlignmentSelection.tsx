import { useField } from 'formik';
import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  display: grid;
  grid-gap: 22px;
  grid-auto-flow: column;
`;

type ButtonProps = {
  selected: boolean;
}

const Button = styled.div<ButtonProps>`
  padding: 13px 50px;
  background: #F1F1F1;
  border-radius: 6px;
  font-size: 16px;
  line-height: 24px;
  color: #999999;
  cursor: pointer;

  ${(props) => props.selected && `
    background: #EBF2FF;
    border: 1px solid #3860AD;
    color: #3860AD;
  `}
`;

const alignments = ['Left', 'Center', 'Right'];

function AlignmentSelection() {
  const [input,, helpers] = useField('alignment');
  return (
    <Container>
      {alignments.map((alignment) => (
        <Button
          selected={input.value === alignment}
          key={alignment}
          onClick={() => helpers.setValue(alignment)}
        >
          {alignment}
        </Button>
      ))}
    </Container>
  );
}

export default AlignmentSelection;
