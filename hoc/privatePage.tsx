import React from 'react';
import { NextPage } from 'next';
import LoginPage from '../pages/login';
import { useAuthContext } from '../hooks/contexts';

export default (Page: NextPage) => function privatePage(props: any) {
  const { auth: { user } } = useAuthContext();
  if (!user) {
    return <LoginPage />;
  }
  return <Page {...props} />;
};
