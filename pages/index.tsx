import React from 'react';
import privatePage from '../hoc/privatePage';
import BrowsePage from './browse';

function IndexPage() {
  return <BrowsePage />;
}

export default privatePage(IndexPage);
