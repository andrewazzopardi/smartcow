const theme = {
  colors: {
    primary: '#46B13D',
    primaryDark: '#34A12D',
    white: '#FFFFFF',
    blue: '#B3CAF7',
    blueDark: '#3860AD',
    red: '#EE6363',
    redText: '#EE6363',
    redDark: '#DE5353',
    redLight: '#FE7373',
    grey: '#F1F1F1',
    greyDark: '#E1E1E1',
    greyText: '#999999',
  },
};

export default theme;
