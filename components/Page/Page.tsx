import styled from 'styled-components';
import { minWidth } from '../../utils/mediaQueries';

const Page = styled.div`
  display: grid;
  grid-template-rows: auto 1fr;
  grid-template-columns: 1fr;
  grid-template-areas:
    'header'
    'content';
  height: 100%;
  background-image: url('/static/background/watermark.svg');
  background-repeat: no-repeat;
  background-position: top right;
`;

type HeaderProps = {
  noBorder?: boolean;
}

const Header = styled.div<HeaderProps>`
  grid-area: header;
  white-space: nowrap;
  height: 100px;
  font-size: 22px;
  font-weight: 500;
  line-height: 33px;
  display: flex;
  flex-wrap: wrap;
  margin: 0 30px;
  padding: 0 10px;
  align-items: center;

  ${(props) => !props.noBorder && `
    border-bottom: 1px solid rgba(0, 0, 0, 0.1);
  `}
`;

const HeaderActions = styled.div`
  display: grid;
  grid-gap: 20px;
  margin-left: auto;
  place-items: center right;
  grid-auto-flow: column;
`;

type ContentProps = {
  centered?: boolean;
};

const Content = styled.div<ContentProps>`
  grid-area: content;
  padding: 20px 30px;
  display: grid;
  ${(props) => props.centered && 'place-items: center;'}
  
  max-width: 100vw;
  overflow: auto;

  ${() => minWidth(600)`
    max-width: calc(100vw - 100px);
  `}
`;

export { Content, Header, HeaderActions };
export default Page;
