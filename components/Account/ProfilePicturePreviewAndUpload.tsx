import { useField } from 'formik';
import React, {
  FormEventHandler, useEffect, useRef, useState,
} from 'react';
import styled from 'styled-components';

const Container = styled.div`
  display: grid;
  grid-gap: 20px;
  grid-auto-flow: column;
  align-items: center;
  justify-content: start;
`;

type ImageProps = {
  url: string | null;
}

const Picture = styled.div<ImageProps>`
  width: 100px;
  height: 100px;
  background-color: #eee;
  background: url(${({ url }) => url});
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;
  border-radius: 50%;
  margin: 40px 0;
`;

const UploadButton = styled.button`
  width: 32px;
  height: 32px;
  border-radius: 50%;
  background-color: #eee;
  border: none;
  background: url("/static/icon/pencil.svg");
  background-size: 75%;
  background-repeat: no-repeat;
  background-position: center;

  &:hover {
    background-color: #6890DD;

    & path {
      fill: #3860AD;
    }
  }
`;

function ProfilePicturePreviewAndUpload() {
  const [{ value },, { setValue }] = useField('userImageUrl');
  const inputRef = useRef<HTMLInputElement>();
  const [images, setImages] = useState<FileList>(null);
  const [imageUrl, setImageUrl] = useState<string | null>(value);

  useEffect(() => {
    if (images) {
      const nextUrl = URL.createObjectURL(images[0]);
      setImageUrl(nextUrl);
      setValue(nextUrl);
    }
  }, [images]);

  const onSelectImage: FormEventHandler = (event) => {
    setImages((event.target as HTMLInputElement).files);
  };

  return (
    <Container>
      <input
        ref={inputRef}
        type="file"
        accept="image/*"
        hidden
        onChange={onSelectImage}
      />
      <Picture url={imageUrl} />
      <UploadButton onClick={() => inputRef.current.click()} />
    </Container>
  );
}

export default ProfilePicturePreviewAndUpload;
