import React from 'react';
import styled from 'styled-components';
import ActorPreview from './Actor';

const Container = styled.div`
  padding: 5px;
  background: #F4F4F4;
  border-radius: 6px;
  display: grid;
  grid-gap: 12px;
`;

const VideoName = styled.div`
  font-size: 16px;
  text-align: center;
`;

const TagList = styled.div`
  display: grid;
  grid-auto-flow: column;
  grid-gap: 6px;
  justify-content: center;
  margin-bottom: 14px;
  min-height: 22px;
`;

const Tag = styled.div`
  font-size: 12px;
  line-height: 14px;
  color: #999999;
  border: 1px solid #999999;
  border-radius: 4px;
  padding: 3px 13px;
`;

type Props = {
  video: Video;
}

function SmallPreview({ video }: Props) {
  return (
    <Container>
      <ActorPreview actorId={video.actorId} />
      <VideoName>
        {video.name}
      </VideoName>
      <TagList>
        {video.tagList.map((tag) => <Tag key={tag}>{tag}</Tag>) }
      </TagList>
    </Container>
  );
}

export default SmallPreview;
