import styled from 'styled-components';

type Props = {
  isBig?: boolean;
}

const UnderlinedText = styled.input<Props>`
  font-size: ${(props) => (props.isBig ? '22px' : '15px')};
  font-weight: ${(props) => (props.isBig ? '500' : '400')};
  line-height: 22px;
  letter-spacing: 0;
  color: #666666;
  background: white;
  border: none;
  border-bottom: 1px solid rgba(0, 0, 0, 0.1);
  max-width: 640px;
  resize: none;

  &:focus {
    outline: none;
    border-color: ${({ theme }) => theme.colors.blueDark};
    color: ${({ theme }) => theme.colors.blueDark};
  }
`;

export default UnderlinedText;
