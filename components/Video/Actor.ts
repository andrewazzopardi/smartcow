import styled from 'styled-components';

type ActorPreviewProps = {
  actorId: number;
  selected?: boolean;
}

const ActorPreview = styled.div<ActorPreviewProps>`
  background-image: url('/static/actors/small/actor_${({ actorId }) => actorId}.png');
  background-size: cover;
  aspect-ratio: 150 / 85;
  cursor: pointer;

  ${({ selected }) => selected && `
    border: 2px solid #3860AD;
  `}
`;

export default ActorPreview;
