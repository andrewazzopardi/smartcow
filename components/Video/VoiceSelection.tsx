import { useField } from 'formik';
import React from 'react';
import styled from 'styled-components';
import Sound from '../Form/Audio/Sound';

const Container = styled.div`
  display: grid;
  grid-gap: 20px;
`;

const voices = [{
  id: 1,
  name: 'Asian',
}, {
  id: 2,
  name: 'British',
}, {
  id: 3,
  name: 'American',
}];

function VoiceSelection() {
  const [input,, helpers] = useField('voiceId');

  return (
    <Container>
      {voices.map(({ name, id }) => (
        <Sound selected={input.value === id} name={name} onClick={() => helpers.setValue(id)} />
      ))}
    </Container>
  );
}

export default VoiceSelection;
