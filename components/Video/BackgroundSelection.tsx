import { useField } from 'formik';
import React, { useState } from 'react';
import styled from 'styled-components';
import Collapsible from '../Collapsible';

const backgroundGroups = [
  {
    id: '1',
    name: 'Images',
    backgrounds: [
      {
        id: '1',
        name: 'Upload',
        url: '/static/backgrounds/images/1.png',
      },
      {
        id: '2',
        name: 'Office',
        url: '/static/backgrounds/images/2.png',
      },
      {
        id: '3',
        name: 'Space',
        url: '/static/backgrounds/images/3.png',
      },
      {
        id: '4',
        name: 'Noise',
        url: '/static/backgrounds/images/4.png',
      },
      {
        id: '5',
        name: 'Meeting Room',
        url: '/static/backgrounds/images/5.png',
      },
      {
        id: '6',
        name: 'Books',
        url: '/static/backgrounds/images/6.png',
      },
      {
        id: '7',
        name: 'Desk',
        url: '/static/backgrounds/images/7.png',
      },
    ],
  },
  {
    id: '2',
    name: 'Solid Colours',
    backgrounds: [
      {
        id: '1',
        name: 'Red',
        url: '/static/backgrounds/solid-colours/1.png',
      },
      {
        id: '2',
        name: 'Blue',
        url: '/static/backgrounds/solid-colours/2.png',
      },
      {
        id: '3',
        name: 'Green',
        url: '/static/backgrounds/solid-colours/3.png',
      },
      {
        id: '4',
        name: 'Yellow',
        url: '/static/backgrounds/solid-colours/4.png',
      },
      {
        id: '5',
        name: 'Orange',
        url: '/static/backgrounds/solid-colours/5.png',
      },
      {
        id: '6',
        name: 'Purple',
        url: '/static/backgrounds/solid-colours/6.png',
      },
      {
        id: '7',
        name: 'Pink',
        url: '/static/backgrounds/solid-colours/7.png',
      },
    ],
  },
  {
    id: '3',
    name: 'Videos',
    backgrounds: [],
  },
];

const Container = styled.div`
  display: grid;
  grid-gap: 15px;
`;

const BackgroundGrid = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(150px, 1fr));
  grid-gap: 15px;
`;

type BackgroundProps = {
  selected: boolean;
  url?: string;
}

const BackgroundOption = styled.div<BackgroundProps>`
  text-align: center;
  ${({ selected }) => selected && `
    color: #3860AD;
  `}
  display: flex;
  align-items: center;
  flex-direction: column;
  cursor: pointer;
`;

const BackgroundPreview = styled.div<BackgroundProps>`
  background-image: url(${({ url }) => url});
  background-size: cover;
  background-position: center;
  width: 150px;
  height: 84px;
  border-radius: 6px;

  ${({ selected }) => selected && `
    border: 2px solid #3860AD;
  `}
`;

function BackgroundSelection() {
  const [{ value },, { setValue }] = useField('background');
  const [backgroundGroup, background] = value.split('/');
  const [activeCollapsible, setActiveCollapsible] = useState(backgroundGroup);

  return (
    <Container>
      {backgroundGroups.map(({ id, name, backgrounds }) => (
        <Collapsible
          title={name}
          active={activeCollapsible === id}
          onToggle={() => setActiveCollapsible(activeCollapsible === id ? 0 : id)}
          key={id}
        >
          <BackgroundGrid>
            {backgrounds.map((backgroundOption) => (
              <BackgroundOption
                selected={backgroundGroup === id && backgroundOption.id === background}
                onClick={() => setValue(`${id}/${backgroundOption.id}`)}
              >
                <BackgroundPreview
                  key={backgroundOption.id}
                  url={backgroundOption.url}
                  selected={backgroundGroup === id && backgroundOption.id === background}
                />
                {backgroundOption.name}
              </BackgroundOption>
            ))}
          </BackgroundGrid>
        </Collapsible>
      ))}
    </Container>
  );
}

export default BackgroundSelection;
