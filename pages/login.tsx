import { Form, Formik } from 'formik';
import React from 'react';
import styled from 'styled-components';
import Button from '../components/Form/Button';
import TextField from '../components/Form/Input/Text';
import Link from '../components/Link';
import Page, { Content, Header } from '../components/Page/Page';
import Spacer from '../components/Spacer';
import publicPage from '../hoc/publicPage';
import { useAuthContext } from '../hooks/contexts';

const StyledForm = styled(Form)`
  display: flex;
  flex-direction: column;
  max-width: 360px;
  width: 100%;
`;

function LoginPage() {
  const { login } = useAuthContext();

  return (
    <Page>
      <Header noBorder>Login</Header>
      <Content centered>
        <Formik
          initialValues={{ email: '', password: '' }}
          onSubmit={(values) => login(values.email, values.password)}
        >
          <StyledForm>
            <TextField
              name="email"
              label="Email"
              placeholder="Email"
            />
            <Spacer height={20} />
            <TextField
              name="password"
              label="Password"
              placeholder="Password"
            />
            <Spacer height={30} />
            <Button type="submit" centered>Login</Button>
            <Spacer height={64} />
            <div style={{ textAlign: 'center' }}>
              New here?
              {' '}
              <Link href="/signup">Sign up</Link>
            </div>
          </StyledForm>
        </Formik>
      </Content>
    </Page>
  );
}

export default publicPage(LoginPage);
