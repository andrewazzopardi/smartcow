import React from 'react';
import styled from 'styled-components';
import { minWidth } from '../../utils/mediaQueries';
import NavBar from './NavBar/NavBar';

const Container = styled.div`
  display: grid;
  grid-template-areas:
    'navbar'
    'content';
  grid-template-rows: 100px 1fr;
  min-height: 100vh;

  ${() => minWidth(600)`
    grid-template-areas:
      'navbar content';
    grid-template-rows: none;
    grid-template-columns: 100px 1fr;
  `}
`;

const Content = styled.div`
  grid-area: content;
`;

interface ILayoutProps {
  children: React.ReactNode;
}

function Layout({ children }: ILayoutProps) {
  return (
    <Container>
      <NavBar />
      <Content>
        {children}
      </Content>
    </Container>
  );
}

export default Layout;
