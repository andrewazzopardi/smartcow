import { useField } from 'formik';
import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  display: grid;
  grid-template-columns: auto auto;
  grid-template-rows: 21px 40px;
  grid-template-areas:
    'label info'
    'input input';
  grid-gap: 6px;
`;

const Label = styled.label`
  grid-area: label;
  font-size: 14px;
  line-height: 21px;
`;

const Info = styled.div`
  grid-area: info;
  font-size: 14px;
  line-height: 21px;
  color: ${({ theme }) => theme.colors.darkBlue};
`;

const Input = styled.input`
  grid-area: input;
  border: 1px solid #999999;
  border-radius: 6px;
  font-size: 15px;
  font-weight: 500;
  line-height: 23px;
  letter-spacing: 0;
  padding: 8px 15px;

  &:focus {
    outline: none;
    border-color: #5C90F3
  }
`;

type Props = {
  label: string;
  placeholder: string;
  name: string;
  info?: string;
}

function TextField({
  label, placeholder, name, info,
}: Props) {
  const [{ onBlur, onChange, value }, { touched, error }] = useField(name);

  return (
    <Container>
      <Label htmlFor={name}>
        {label}
      </Label>
      <Info>
        {(touched && error) || info}
      </Info>
      <Input
        id={name}
        name={name}
        value={value}
        onChange={onChange}
        onBlur={onBlur}
        placeholder={placeholder}
      />
    </Container>
  );
}

export default TextField;
