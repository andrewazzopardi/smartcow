import React from 'react';
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { useAuthContext } from '../hooks/contexts';

export default (Page: NextPage) => function publicPage(props: any) {
  const router = useRouter();
  const nextUrl = router.query.nextUrl as string;
  const { auth: { user } } = useAuthContext();

  if (user) {
    router.replace(nextUrl || '/');
  }

  return <Page {...props} />;
};
