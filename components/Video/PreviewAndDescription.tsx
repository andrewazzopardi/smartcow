import { useField } from 'formik';
import React from 'react';
import styled from 'styled-components';
import Button from '../Form/Button';

const Container = styled.div`
  align-self: start;
  background: #F4F4F4;
  border-radius: 20px;
  padding: 20px;
  display: grid;
  grid-gap: 20px;
  position: relative;
`;

type PreviewCardProps = {
  actorId: number;
}

const PreviewCard = styled.div<PreviewCardProps>`
  background-image:
    url("/static/actors/big/actor_${(props) => props.actorId}.png"),
    url("/static/actors/small/actor_${(props) => props.actorId}.png");
  background-position: center;
  background-size: cover;
  background-size: cover;
  border-radius: 20px;
  aspect-ratio: 7 / 4;
`;

const DescriptionCard = styled.textarea`
  background: ${({ theme }) => theme.colors.white};
  padding: 20px 30px;
  border: none;
  resize: none;
  min-height: 188px;  
`;

const ListenButton = styled(Button)`
  position: absolute;
  bottom: 40px;
  left: 40px;
`;

function PreviewAndDescription() {
  const [{ value: actorId }] = useField<number>('actorId');

  return (
    <Container>
      <PreviewCard actorId={actorId} />
      <DescriptionCard placeholder="Type or paste your videoscript here. You can also request a translation of an English script to any of 27 other languages" />
      <ListenButton color="grey" type="button">Listen</ListenButton>
    </Container>
  );
}

export default PreviewAndDescription;
