/* eslint-disable react/jsx-props-no-spreading */
import { AppProps } from 'next/app';
import React from 'react';
import { createGlobalStyle, ThemeProvider } from 'styled-components';
import Layout from '../components/Layout/Layout';
import AuthProvider from '../components/Provider/Auth';
import theme from '../utils/theme';

const GlobalStyle = createGlobalStyle`
  html {
    overflow-y: auto;
  }  

  body {
    margin: 0;
    padding: 0;
  }

  * {
    font-family: 'Poppins', sans-serif;
    box-sizing: border-box;
  }
`;

function App({ Component, pageProps }: AppProps) {
  return (
    <>
      <GlobalStyle />
      <ThemeProvider theme={theme}>
        <AuthProvider>
          <Layout>
            <Component {...pageProps} />
          </Layout>
        </AuthProvider>
      </ThemeProvider>
    </>
  );
}

export default App;
