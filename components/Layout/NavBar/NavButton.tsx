import Image from 'next/image';
import Link from 'next/link';
import React, { CSSProperties } from 'react';
import styled from 'styled-components';

interface IActivatable {
  isActive?: boolean;
}

interface IContainerProps extends IActivatable {
  noHover?: boolean;
}

const Container = styled.div<IContainerProps>`
  width: 60px;
  height: 60px;
  border-radius: 10px;
  background: ${(props) => (props.isActive ? 'rgba(235, 242, 255, 1)' : 'none')};
  position: relative;
  display: grid;
  place-items: center;
  cursor: pointer;

  ${(props) => !props.noHover && !props.isActive && `
    &:hover {
      background: rgba(235, 242, 255, 0.7);
    }
  `}
`;

export interface INavButtonProps {
  imageUrl?: string;
  onClick?: () => void;
  isActive?: boolean;
  width?: number;
  height?: number;
  noHover?: boolean;
  href?: string;
  style?: CSSProperties;
  // eslint-disable-next-line react/no-unused-prop-types
  withAuth?: boolean;
}

function NavButton({
  imageUrl, onClick, isActive, width, height, noHover, href, style,
}: INavButtonProps) {
  return (
    <Link href={href} passHref>
      <Container
        onClick={onClick}
        isActive={isActive}
        noHover={noHover}
        style={style}
      >
        <Image
          src={imageUrl}
          alt="me"
          width={width}
          height={height}
        />
      </Container>
    </Link>
  );
}

export default NavButton;
