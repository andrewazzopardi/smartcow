import { useRouter } from 'next/router';
import React from 'react';
import styled from 'styled-components';
import { useAuthContext } from '../../../hooks/contexts';
import { minWidth } from '../../../utils/mediaQueries';
import NavButton, { INavButtonProps } from './NavButton';

const Container = styled.div`
  grid-area: navbar;
  position: sticky;
  top: 0;
  left: 0;
  padding: 20px;
  display: grid;
  grid-gap: 20px;
  grid-template-columns: auto auto auto 1fr auto;
  z-index: 1;
  max-width: 100vw;

  &::before {
    content: '';
    position: absolute;
    top: 0;
    right: 40px;
    bottom: 0;
    left: 40px;
    z-index: 0;
    box-shadow: 0 0 0 0 black;
    transition: box-shadow 300ms, right 300ms, left 300ms, top 300ms, bottom 300ms;
  }

  ::after {
    background: rgba(0,0,0,0.1);
    position: absolute;
    right: 40px;
    bottom: 0;
    left: 40px;
    height: 2px;
    content: '';
    z-index: 2;
    transition: right 300ms, left 300ms, top 300ms, bottom 300ms;
  }

  &:hover {
    &::after {
      right: 0;
      left: 0;
    }

    &::before {
      box-shadow: 0 0 8px 0 #0007;
      right: 0;
      left: 0;
    }
  }

  ${() => minWidth(600)`
    grid-template-columns: unset;
    grid-template-rows: auto auto auto 1fr auto;
    max-width: unset;
    max-height: 100vh;
    grid-auto-flow: unset;
    grid-auto-columns: unset;
    grid-auto-rows: 60px;

    ::after {
      top: 40px;
      right: 0;
      bottom: 40px;
      left: unset;
      height: unset;
      width: 2px;
    }

    &::before {
      top: 40px;
      right: 0;
      bottom: 40px;
      left: 0;
    }

    &:hover {
      &::after {
        top: 0;
        bottom: 0;
      }
  
      &::before {
        box-shadow: 0 0 8px 0 #0007;
        top: 0;
        bottom: 0;
      }
    }
  `}
`;

const Background = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  left: 0;
  bottom: 0;
  background: white;
`;

const navBarButtons: INavButtonProps[] = [
  {
    imageUrl: '/static/logo/small.svg',
    width: 28,
    height: 28,
    noHover: true,
    href: '/browse',
  },
  {
    imageUrl: '/static/icon/video.svg',
    width: 28,
    height: 28,
    href: '/video',
    withAuth: true,
  },
  {
    imageUrl: '/static/icon/browse.svg',
    width: 44,
    height: 44,
    href: '/browse',
    withAuth: true,
  },
  {},
  {
    imageUrl: '/static/account/profilePicture.png',
    width: 44,
    height: 44,
    href: '/account',
    withAuth: true,
  },
];

function NavBar() {
  const router = useRouter();
  const { auth: { user } } = useAuthContext();

  return (
    <Container>
      <Background />
      {navBarButtons.map((button) => (!button.withAuth || user) && (button.href ? (
        <NavButton
          key={button.noHover ? 'home' : button.href}
          {...button}
          isActive={!button.noHover && router.asPath.indexOf(button.href) === 0}
        />
      ) : (
        <div
          key={button.noHover ? 'home' : button.href}
        />
      )))}
    </Container>
  );
}

export default NavBar;
