import React from 'react';

export type Props = {
  // eslint-disable-next-line react/no-unused-prop-types
  name: string;
  children: React.ReactNode;
}

function Tab({ children }: Props) {
  return (
    <div>
      {children}
    </div>
  );
}

export default Tab;
