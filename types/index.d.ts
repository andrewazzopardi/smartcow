type User = {
  id: number;
  email: string;
  password: string;
  firstName: string;
  lastName: string;
  planId: number;
}

type Auth = {
  user?: User
}
