import { Form, Formik } from 'formik';
import Image from 'next/image';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import Button from '../../components/Form/Button';
import UnderlinedText from '../../components/Form/Input/UnderlinedText';
import Page, { Content, Header, HeaderActions } from '../../components/Page/Page';
import Spacer from '../../components/Spacer';
import TagList from '../../components/TagList/TagList';
import PreviewAndDescription from '../../components/Video/PreviewAndDescription';
import VideoConfigurations from '../../components/Video/VideoConfigurations';
import privatePage from '../../hoc/privatePage';
import { useAuthContext } from '../../hooks/contexts';
import { minWidth } from '../../utils/mediaQueries';
import { addVideo, getVideo, updateVideo } from '../../utils/store';

const WhiteBackground = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: white;
  opacity: 0.9;
  z-index: 1;
  padding-top: 100px;

  ${() => minWidth(600)`
    padding-top: 0;
    padding-left: 100px;
  `}
`;

const VideoName = styled.div``;

const StyledContent = styled(Content)`
  display: grid;
  grid-template-columns: 740fr 510fr;
  grid-gap: 20px;
`;

const initialValues: Video = {
  id: 0,
  userId: 0,
  name: '',
  description: '',
  tagList: [],
  actorId: 1,
  script: '',
  voiceId: 1,
  alignment: 'Center',
  background: '2/1',
};

function VideoPage() {
  const router = useRouter();
  const { auth: { user: { id } } } = useAuthContext();
  const videoId = Number((router.query.videoId as string) || 0);
  const [displayDetailsPane, setDisplayDetailsPane] = useState(false);

  useEffect(() => {
    if (router.isReady) {
      setDisplayDetailsPane(videoId === 0);
    }
  }, [router.isReady, videoId]);

  const onSubmit = (values: Video) => {
    if (videoId) {
      updateVideo(values);
    } else {
      addVideo(values);
    }
    router.push('/browse');
  };

  const video = getVideo(videoId);

  return (
    <Page>
      <Formik
        initialValues={video || { ...initialValues, userId: id }}
        enableReinitialize
        onSubmit={onSubmit}
      >
        {({ values, handleChange }) => (
          <Form>
            <Header>
              {!displayDetailsPane && (
              <VideoName onClick={() => setDisplayDetailsPane(true)}>
                {values.name || 'Unnamed Video'}
                {' '}
                <Image src="/static/icon/chevron-down.svg" width={10} height={10} />
              </VideoName>
              )}
              <HeaderActions>
                <Button type="reset" color="grey" onClick={() => setDisplayDetailsPane(true)}>
                  Cancel
                </Button>
                <Button type="submit">
                  Save
                </Button>
              </HeaderActions>
            </Header>
            <StyledContent>
              <PreviewAndDescription />
              <VideoConfigurations />
            </StyledContent>
            {displayDetailsPane && (
              <WhiteBackground>
                <Header noBorder>
                  <UnderlinedText
                    name="name"
                    placeholder="Video title"
                    value={values.name}
                    onChange={handleChange}
                    isBig
                  />
                </Header>
                <Content>
                  <UnderlinedText
                    name="description"
                    placeholder="Write a description here..."
                    as="textarea"
                    value={values.description}
                    onChange={handleChange}
                    onInput={(event: any) => {
                      const element = event.target;
                      element.style.height = '5px';
                      element.style.height = `${element.scrollHeight + 1}px`;
                    }}
                  />
                  <Spacer height={20} />
                  <TagList name="tagList" />
                  <Spacer height={14} />
                  <div style={{ display: 'flex' }}>
                    <Button onClick={() => setDisplayDetailsPane(false)}>
                      Save
                    </Button>
                  </div>
                </Content>
              </WhiteBackground>
            )}
          </Form>
        )}
      </Formik>
    </Page>
  );
}

export default privatePage(VideoPage);
