import React from 'react';
import styled from 'styled-components';

type ContainerProps = {
  canDelete: boolean;
}

const Container = styled.div<ContainerProps>`
  border: 1px solid #999999;
  border-radius: 6px;
  margin: 0 10px 10px 0;
  display: flex;
  flex-wrap: nowrap;
  transition: border-color 300ms ease-in-out, color 300ms ease-in-out;
  height: 27px;
  max-width: fit-content;

  &:focus {
    outline: none;
    border-color: ${({ theme }) => theme.colors.blueDark};
  }

  ${({ canDelete }) => canDelete && `
    &:hover {
      cursor: pointer;
      border-color: ${({ theme }) => theme.colors.red};
      color: ${({ theme }) => theme.colors.red};

      & .deleteButton {
        width: 24px;
      }
    }
  `}
`;

const Content = styled.div`
  padding: 3px 13px;
  font-size: 14px;
  text-align: center;
`;

const DeleteButton = styled.div`
  width: 0;
  transition: width 300ms ease-in-out;
  background: ${({ theme }) => theme.colors.red};
  background: url('/static/icon/cross.svg') no-repeat center;
  background-size: 20px 20px;
`;

type Props = {
  children?: string;
  onDelete?: () => void;
  onCreate?: (input: string) => void;
  isNew?: boolean;
}

function Tag({
  children, onDelete, onCreate, isNew,
}: Props) {
  const canDelete = onDelete !== undefined;
  const defaultText = 'Create a tag';

  const handleKeyUp: React.KeyboardEventHandler<HTMLDivElement> = (event) => {
    const target = event.target as HTMLInputElement;
    if ((event.key === 'Enter') && target.innerHTML) {
      event.stopPropagation();
      event.preventDefault();
      onCreate?.(target.innerHTML);
      target.innerHTML = '';
    }
  };

  const handleFocus: React.FocusEventHandler<HTMLDivElement> = (event) => {
    const target = event.target as HTMLDivElement;
    target.innerHTML = '';
  };

  const handleBlur: React.FocusEventHandler<HTMLDivElement> = (event) => {
    const target = event.target as HTMLDivElement;
    if (target.innerHTML) {
      onCreate?.(target.innerHTML);
    }
    target.innerHTML = defaultText;
  };

  return (
    <Container
      canDelete
      onClick={onDelete}
    >
      <Content
        contentEditable={isNew}
        onFocus={handleFocus}
        onBlur={handleBlur}
        onKeyUp={handleKeyUp}
        onKeyDown={(event) => event.key === 'Enter' && event.preventDefault()}
        suppressContentEditableWarning
        dangerouslySetInnerHTML={{ __html: isNew ? defaultText : children }}
        style={{ minWidth: isNew ? '108px' : undefined }}
      />
      {canDelete && <DeleteButton className="deleteButton" />}
    </Container>
  );
}

export default Tag;
