import { Form, Formik } from 'formik';
import React from 'react';
import styled from 'styled-components';
import { useAuthContext } from '../../hooks/contexts';
import Button from '../Form/Button';
import TextField from '../Form/Input/Text';
import ProfilePicturePreviewAndUpload from './ProfilePicturePreviewAndUpload';

const Container = styled.div``;

const GridForm = styled(Form)`
  display: grid;
  grid-gap: 20px;
  grid-auto-flow: auto;
  grid-template-columns: repeat(auto-fit, minmax(280px, 1fr));
  max-width: 720px;
`;

function ProfileSettings() {
  const { auth: { user }, updateUser } = useAuthContext();

  return (
    <Container>
      <Formik initialValues={user} onSubmit={updateUser}>
        <GridForm>
          <ProfilePicturePreviewAndUpload />
          <div />
          <TextField
            label="First Name"
            name="firstName"
            placeholder="First Name"
          />
          <TextField
            label="Last Name"
            name="lastName"
            placeholder="Last Name"
          />
          <TextField
            label="Email"
            name="email"
            placeholder="Email"
          />
          <div />
          <Button type="submit" style={{ marginTop: '20px', width: '150px' }}>
            Save Changes
          </Button>
        </GridForm>
      </Formik>
    </Container>
  );
}

export default ProfileSettings;
