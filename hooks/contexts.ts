import React, { createContext } from 'react';
import { NoOp } from '../utils/misc';

const AuthContext = createContext<AuthContext>({
  auth: {
    user: null,
  },
  isLoading: true,
  login: NoOp,
  signup: NoOp,
  logout: NoOp,
  updateUser: NoOp,
});

const useAuthContext = () => React.useContext(AuthContext);

export {
  AuthContext,
  useAuthContext,
};
