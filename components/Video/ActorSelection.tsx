import { useField } from 'formik';
import React from 'react';
import styled from 'styled-components';
import ActorPreview from './Actor';

const availableActors = [
  {
    id: 1,
    name: 'Anna',
  },
  {
    id: 2,
    name: 'YoYo',
  },
  {
    id: 3,
    name: 'Skye',
  },
  {
    id: 4,
    name: 'Mike',
  },
  {
    id: 5,
    name: 'Vincent',
  },
  {
    id: 6,
    name: 'Peter',
  },
  {
    id: 7,
    name: 'May',
  },
];

const Container = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(150px, 1fr));
  grid-gap: 10px;
`;

const ActorName = styled.div`
  margin-top: 3px;
  text-align: center;
`;

function ActorSelection() {
  const [{ value },, { setValue }] = useField('actorId');

  return (
    <Container>
      {availableActors.map(({ name, id }) => (
        <div key={id}>
          <ActorPreview
            actorId={id}
            selected={id === value}
            onClick={() => setValue(id)}
          />
          <ActorName>{name}</ActorName>
        </div>
      ))}
    </Container>
  );
}

export default ActorSelection;
