import { acceptTemplate } from './templateString';

/* eslint-disable import/prefer-default-export */
const minWidth = (width: number) => (...templateString: any[]) => `
  @media only screen and (min-width: ${width}px) {
    ${acceptTemplate(templateString)}
  }
`;

export { minWidth };
