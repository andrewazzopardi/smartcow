function getLocalStorage<T>(key: string, defaultValue: T) {
  const value = localStorage.getItem(key);
  return value ? JSON.parse(value) as T : defaultValue;
}

function getVideos(userId?: number) {
  return getLocalStorage<Video[]>('videos', []).filter((v) => !userId || v.userId === userId);
}

function getVideo(id: number) {
  const videos = getVideos();
  return videos.find((v) => v.id === id);
}

function addVideo(video: Video) {
  const videos = getVideos();
  videos.push({ ...video, id: videos.length + 1 });
  localStorage.setItem('videos', JSON.stringify(videos));
}

function updateVideo(video: Video) {
  const videos = getVideos();
  const index = videos.findIndex((v) => v.id === video.id);
  if (index >= 0) {
    videos[index] = video;
    localStorage.setItem('videos', JSON.stringify(videos));
  }
}

function getUsers() {
  return getLocalStorage<User[]>('users', []);
}

function registerUser(user: User) {
  const users = getUsers();
  users.push({ ...user, id: users.length + 1 });
  localStorage.setItem('users', JSON.stringify(users));
  return user;
}

function getUser(email: string, password: string) {
  const users = getUsers();
  return users.find((u) => u.email === email && u.password === password);
}

function updateUser(user: User) {
  const users = getUsers();
  const index = users.findIndex((u) => u.id === user.id);
  if (index >= 0) {
    users[index] = user;
    localStorage.setItem('users', JSON.stringify(users));
  }
  return user;
}

export {
  getVideos, getVideo, addVideo, updateVideo, registerUser, getUser, updateUser,
};
