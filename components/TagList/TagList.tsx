import { useField } from 'formik';
import React from 'react';
import styled from 'styled-components';
import Tag from './Tag';

const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

type Props = {
  name: string;
}

function TagList({ name }: Props) {
  const [input, , helpers] = useField<string[]>(name);

  const onDelete = (index: number) => () => {
    const nextValue = [...input.value];
    nextValue.splice(index, 1);
    helpers.setValue(nextValue);
  };

  const onCreate = (text: string) => {
    if (input.value.includes(text)) {
      return;
    }
    const nextValue = [...input.value, text];
    helpers.setValue(nextValue);
  };

  return (
    <Container>
      <Tag isNew onCreate={onCreate} />
      {input.value.map((tag, index) => (
        <Tag key={tag} onDelete={onDelete(index)}>{tag}</Tag>
      ))}
    </Container>
  );
}

export default TagList;
