import React, { useState } from 'react';
import styled from 'styled-components';
import { Props as TabProps } from './Tab';

const Container = styled.div`
  display: grid;
  grid-gap: 30px;
  grid-template-rows: auto 1fr;
  min-width: 0;
`;

const TabSelection = styled.div`
  display: grid;
  grid-gap: 30px;
  font-weight: 500;
  font-size: 15px;
  line-height: 22px;
  color: #999999;
  grid-auto-flow: column;
  overflow-x: auto;
  max-width: fit-content;
  position: sticky;
  left: 0;
`;

type TabButtonProps = {
  selected: boolean;
}

const TabButton = styled.div<TabButtonProps>`
  cursor: pointer;
  ${({ selected }) => selected && `
    color: #3860AD;
  `}
`;

const TabContent = styled.div`
  min-width: 0;
`;

type Props = {
  children: React.ReactElement<TabProps>[];
}

function Tabs({ children }: Props) {
  const [selectedTab, setSelectedTab] = useState(0);

  return (
    <Container>
      <TabSelection>
        {React.Children.map(children, (child, index) => (
          <TabButton
            key={child.props.name}
            onClick={() => setSelectedTab(index)}
            selected={index === selectedTab}
          >
            {child.props.name}
          </TabButton>
        ))}
      </TabSelection>
      <TabContent>
        {children[selectedTab]}
      </TabContent>
    </Container>
  );
}

export default Tabs;
