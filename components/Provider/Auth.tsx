import React, {
  useCallback, useEffect, useMemo, useState,
} from 'react';
import { AuthContext } from '../../hooks/contexts';
import { getUser, registerUser, updateUser as editUser } from '../../utils/store';

type Props = {
  children: React.ReactNode;
}

function AuthProvider({ children }: Props) {
  const [auth, setAuth] = useState<Auth>({});
  const [isLoading, setIsLoading] = useState<boolean>(true);

  useEffect(() => {
    const authString = localStorage.getItem('auth');
    if (authString) {
      setAuth(JSON.parse(authString));
    }
    setIsLoading(false);
  }, []);

  const updateAuthAndStorage = useCallback((newAuth: Auth) => {
    localStorage.setItem('auth', JSON.stringify(newAuth));
    setAuth(newAuth);
  }, [setAuth]);

  const login = useCallback((email: string, password: string) => {
    const nextAuth: Auth = {
      user: getUser(email, password),
    };
    updateAuthAndStorage(nextAuth);
  }, [setAuth]);

  const signup = useCallback((fullname: string, email: string, password: string) => {
    const [firstName, lastName] = fullname.split(' ');
    const user = registerUser({
      id: 0,
      firstName,
      lastName,
      email,
      password,
      planId: 1,
    });
    const nextAuth: Auth = { user };
    updateAuthAndStorage(nextAuth);
  }, [setAuth]);

  const logout = useCallback(() => {
    const nextAuth: Auth = {};
    updateAuthAndStorage(nextAuth);
  }, [setAuth]);

  const updateUser = useCallback((user: Partial<User>) => {
    const nextUser = editUser({ ...auth.user, ...user });
    const nextAuth: Auth = {
      user: nextUser,
    };
    updateAuthAndStorage(nextAuth);
  }, [setAuth]);

  const value = useMemo<AuthContext>(() => ({
    auth,
    login,
    signup,
    logout,
    isLoading,
    updateUser,
  }), [auth, login, signup]);

  return (
    <AuthContext.Provider value={value}>
      {isLoading ? null : children}
    </AuthContext.Provider>
  );
}

export default AuthProvider;
