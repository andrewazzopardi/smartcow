import React from 'react';
import styled from 'styled-components';
import { useAuthContext } from '../../hooks/contexts';

type Plan = {
  id: number;
  name: string;
  features: string[];
  nonFeatures?: string[];
  price: number;
}

const plans: Plan[] = [
  {
    id: 1,
    name: 'Free',
    price: 0,
    features: [
      'Pellentesque interdum libero et',
      'Pellentesque posuere jdfkdfkdfhd',
      'Cras sed felis eget',
      'Maecenas eget luctus',
      'Nullam vitae augue',
    ],
  }, {
    id: 2,
    name: 'Pro',
    price: 12,
    features: [
      'Maecenas eget luctus purus',
      'Graesent in sollicitudin velit',
      'Donec in orci vitae nisi ',
      'Class aptent taciti',
    ],
    nonFeatures: [
      'Ut blandit vestibulum',
    ],
  }, {
    id: 3,
    name: 'Team',
    price: 23,
    features: [
      'Etiam ac finibus nisi, a porttitor',
      'Quisque tincidunt velit a sapien vulputate',
      'Vivamus pulvinar ',
      'In hac habitasse platea',
      'Nullam vitae augue',
    ],
    nonFeatures: [
      'Nullam vitae augue',
    ],
  }, {
    id: 4,
    name: 'Agency',
    price: 43,
    features: [
      'Praesent in sollicitudin velit',
      'Nulla tincidunt finibus interdum',
      'Nullam vitae augue',
      'Curabitur eleifend',
      'Quisque vel ex enim',
    ],
  },
];

const Container = styled.div`
  display: grid;
  grid-gap: 32px;
  grid-template-columns: repeat(auto-fill, 280px);
  justify-content: space-evenly;
`;

interface ISelectable {
  selected: boolean;
}

const PlanContainer = styled.div<ISelectable>`
  display: grid;
  grid-gap: 12px;
  padding: 24px;
  border-radius: 10px;
  color: #181059;
  
  ${({ selected }) => selected && `
    background: #46B13D;
    color: white;
  `}
`;

const PlanName = styled.div`
  margin-bottom: 8px;
  font-weight: 600;
  font-size: 22px;
  line-height: 33px;
`;

type FeatureProps = {
  available?: boolean;
};

const Feature = styled.div<FeatureProps>`
  position: relative;
  margin-left: 28px;
  
  ::before {
    position: absolute;
    left: -28px;
    content: '${({ available }) => (available ? '✓' : '✗')}';
  }
`;

const PlanPrice = styled.div`
  font-weight: 700;
  font-size: 30px;
  line-height: 45px;
`;

const PlanButton = styled.div<ISelectable>`
  background: #EBF2FF;
  border-radius: 6px;
  display: grid;
  place-items: center;
  padding: 12px;
  font-weight: 600;
  font-size: 16px;
  color: #3860AD;
  cursor: pointer;

  ${({ selected }) => selected && `
    color: #1C7714;
    background: transparent;
  `}
`;

function MyPlan() {
  const { auth: { user }, updateUser } = useAuthContext();
  const { planId } = user;

  return (
    <Container>
      {plans.map((plan) => (
        <PlanContainer
          key={plan.name}
          selected={planId === plan.id}
        >
          <PlanName>
            {plan.name}
          </PlanName>
          {plan.features.map((feature) => (
            <Feature available key={feature}>
              {feature}
            </Feature>
          ))}
          {plan.nonFeatures?.map((feature) => (
            <Feature key={feature}>
              {feature}
            </Feature>
          ))}
          <PlanPrice>
            $
            {plan.price}
          </PlanPrice>
          <PlanButton selected={planId === plan.id} onClick={() => updateUser({ planId: plan.id })}>
            {planId === plan.id && 'Current Plan'}
            {planId < plan.id && 'Upgrade'}
            {planId > plan.id && 'Downgrade'}
          </PlanButton>
        </PlanContainer>
      ))}
    </Container>
  );
}

export default MyPlan;
