import { Form, Formik } from 'formik';
import React from 'react';
import styled from 'styled-components';
import Button from '../components/Form/Button';
import TextField from '../components/Form/Input/Text';
import Link from '../components/Link';
import Page, { Content, Header } from '../components/Page/Page';
import Spacer from '../components/Spacer';
import publicPage from '../hoc/publicPage';
import { useAuthContext } from '../hooks/contexts';

const StyledForm = styled(Form)`
  display: flex;
  flex-direction: column;
  max-width: 360px;
  width: 100%;
`;

function SignUpPage() {
  const { signup } = useAuthContext();

  return (
    <Page>
      <Header noBorder>Create an account</Header>
      <Content centered>
        <Formik
          initialValues={{ fullname: '', email: '', newPassword: '' }}
          onSubmit={(values) => signup(values.fullname, values.email, values.newPassword)}
        >
          <StyledForm>
            <TextField
              name="fullname"
              label="Full name"
              placeholder="Full name"
            />
            <Spacer height={20} />
            <TextField
              name="email"
              label="Email address"
              placeholder="Email"
            />
            <Spacer height={20} />
            <TextField
              name="newPassword"
              label="New Password"
              placeholder="New Password"
            />
            <Spacer height={30} />
            <Button type="submit" centered>Sign up</Button>
            <Spacer height={64} />
            <div style={{ textAlign: 'center' }}>
              Already a user?
              {' '}
              <Link href="/login">Login</Link>
            </div>
          </StyledForm>
        </Formik>
      </Content>
    </Page>
  );
}

export default publicPage(SignUpPage);
