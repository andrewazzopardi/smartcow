import React from 'react';
import styled from 'styled-components';
import Tab from '../Tabs/Tab';
import Tabs from '../Tabs/Tabs';
import ActorSelection from './ActorSelection';
import AlignmentSelection from './AlignmentSelection';
import BackgroundSelection from './BackgroundSelection';
import VoiceSelection from './VoiceSelection';

const Container = styled.div`
  padding: 20px;
`;

function VideoConfigurations() {
  return (
    <Container>
      <Tabs>
        <Tab name="Actor">
          <ActorSelection />
        </Tab>
        <Tab name="Voice">
          <VoiceSelection />
        </Tab>
        <Tab name="Alignment">
          <AlignmentSelection />
        </Tab>
        <Tab name="Background">
          <BackgroundSelection />
        </Tab>
      </Tabs>
    </Container>
  );
}

export default VideoConfigurations;
