import Image from 'next/image';
import React from 'react';
import styled from 'styled-components';

interface IActivatable {
  active: boolean;
}

const Container = styled.div<IActivatable>`
  padding: 15px;
  display: grid;
  grid-gap: 15px;
  border-radius: 6px;
  background: #EBF2FF00;
  ${({ active }) => !active && `
    transition: background 0.25s 0.25s ease-in-out;
  `}

  ${({ active }) => active && `
    background: #EBF2FFFF;
  `}
`;

const Header = styled.div<IActivatable>`
  font-weight: 500;
  font-size: 14px;
  line-height: 21px;
  ${({ active }) => active && `
    color: #3860AD;
  `}
  display: flex;
  justify-content: space-between;
  align-items: center;
  cursor: pointer;
`;

const Content = styled.div<IActivatable>`
  transition: max-height 0.5s ease-in-out;
  max-height: ${(props) => (props.active ? '350px' : '0')};
  overflow: hidden;
`;

type Props = {
  title: string;
  children: React.ReactNode;
  active: boolean;
  onToggle: () => void;
}

function Collapsible({
  children, title, active, onToggle,
}:Props) {
  return (
    <Container active={active}>
      <Header active={active} onClick={onToggle}>
        <div>
          {title}
        </div>
        <Image
          style={{
            transition: 'transform 0.25s ease-in-out',
            transform: active ? 'rotateX(180deg)' : 'rotateX(0deg)',
          }}
          src="/static/icon/chevron-down.svg"
          width={10}
          height={10}
        />
      </Header>
      <Content active={active}>
        {children}
      </Content>
    </Container>
  );
}

export default Collapsible;
