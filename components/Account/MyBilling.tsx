import Image from 'next/image';
import React from 'react';
import styled from 'styled-components';

const invoices = [{
  referenceId: '#1',
  date: new Date(1, 1, 1),
  amount: 12,
}, {
  referenceId: '#2',
  date: new Date(1, 2, 1),
  amount: 24,
}, {
  referenceId: '#3',
  date: new Date(1, 3, 1),
  amount: 36,
}];

const Table = styled.table`
  border-collapse: collapse;

  & > thead > tr > th {
    font-weight: 600;
    font-size: 14px;
    line-height: 24px;
    text-transform: uppercase;
    color: #222222;
    opacity: 0.5;
  }
`;

const Row = styled.tr`
`;

const Cell = styled.td`
  padding: 18px;
  font-weight: 500;
  font-size: 15px;
  line-height: 24px;
  color: #222222;
  text-align: left;
`;

const columns = [
  {
    header: 'Reference ID',
    accessor: (cell) => cell.referenceId,
  }, {
    header: 'Date',
    accessor: (cell) => cell.date.toLocaleDateString(),
  }, {
    header: 'Amount',
    accessor: (cell) => cell.amount,
  }, {
    header: 'Invoice',
    accessor: () => (
      <a href="https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf" target="_blank" download rel="noreferrer">
        <Image
          src="/static/icon/pdf.svg"
          width={24}
          height={24}
        />
      </a>
    ),
  },
];

function MyBilling() {
  return (
    <Table>
      <thead>
        <Row>
          {columns.map((column) => (
            <Cell as="th" key={column.header}>{column.header}</Cell>
          ))}
        </Row>
      </thead>
      <tbody>
        {invoices.map((invoice) => (
          <Row key={invoice.referenceId}>
            {columns.map((column) => (
              <Cell key={column.header}>{column.accessor(invoice)}</Cell>
            ))}
          </Row>
        ))}
      </tbody>
    </Table>
  );
}

export default MyBilling;
