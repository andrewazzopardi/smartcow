import React from 'react';
import styled from 'styled-components';
import NextLink from 'next/link';

const StyledA = styled.a`
  text-decoration: none;
  color: ${(props) => props.theme.colors.blueDark};
`;

type Props = {
  href: string;
  children: React.ReactNode;
};

function Link({ href, children }: Props) {
  return (
    <NextLink href={href} passHref>
      <StyledA>
        {children}
      </StyledA>
    </NextLink>
  );
}

export default Link;
