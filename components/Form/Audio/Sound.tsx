import React from 'react';
import styled from 'styled-components';
import Track from './Track';

interface ISelectable {
  selected?: boolean;
}

const Container = styled.div<ISelectable>`
  padding: 15px;
  display: grid;
  grid-template-areas:
    'button name'
    'button track';
  grid-template-rows: 10px 30px;
  background: #F1F1F1;
  border-radius: 6px;
  grid-gap: 0 20px;
  box-size: border-box;
  
  ${({ selected }) => selected && `
    background: #EBF2FF;
    border: 1px solid #3860AD;
  `}
`;

const Button = styled.div<ISelectable>`
  grid-area: button;
  border-radius: 50%;
  width: 40px;
  height: 40px;
  background: #3860AD;
  
  ${({ selected }) => selected && `
    background: #3860AD;
  `}
`;

const Name = styled.div<ISelectable>`
  grid-area: name;
  font-weight: 500;
  font-size: 12px;
  line-height: 18px;
  
  ${({ selected }) => selected && `
    color: #3860AD;
  `}
`;

type Props = {
  name: string;
  selected?: boolean;
  onClick: () => void;
}

function Sound({ name, selected, onClick }: Props) {
  return (
    <Container onClick={onClick} selected={selected}>
      <Button selected={selected} />
      <Name selected={selected}>{name}</Name>
      <Track selected={selected} />
    </Container>
  );
}

export default Sound;
