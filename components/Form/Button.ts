import styled from 'styled-components';

type Props = {
  centered?: boolean;
  noBackground?: boolean;
}

const Button = styled.button<Props>`
  background: ${(props) => props.theme.colors[props.color || 'primary']};
  ${(props) => props.noBackground && 'background: none'};
  color: ${(props) => props.theme.colors[`${props.color}Text`] || props.theme.colors.white};
  text-align: center;
  border: none;
  border-radius: 6px;
  padding: 6px 16px;
  font-size: 16px;
  line-height: 21px;
  font-weight: 600;
  cursor: pointer;
  transition: all 0.2s ease-in-out;
  ${(props) => props.centered && 'margin: 0 auto;'}
  height: 33px;

  &:hover {
    background: ${(props) => props.theme.colors[`${props.color}Dark`] || props.theme.colors.primaryDark};
    ${(props) => props.noBackground && `background: ${props.theme.colors.greyDark}`};
  }
`;

export default Button;
