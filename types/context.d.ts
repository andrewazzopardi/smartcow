type AuthContext = {
  auth?: Auth;
  login: (email: string, password: string) => void;
  signup: (fullname: string, email: string, password: string) => void;
  logout: () => void;
  isLoading: boolean;
  updateUser: (user: Partial<User>) => void;
}
